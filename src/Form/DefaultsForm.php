<?php

namespace Drupal\node_type_defaults\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Entity\Menu;
use Drupal\system\MenuInterface;

/**
 * Configure OpenAI client settings for this site.
 */
class DefaultsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_type_defaults';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['node_type_defaults.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['preview_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Preview before submitting'),
      '#default_value' => $this->config('node_type_defaults.settings')->get('preview_mode') ?? 0,
      '#options' => [
        DRUPAL_DISABLED => $this->t('Disabled'),
        DRUPAL_OPTIONAL => $this->t('Optional'),
        DRUPAL_REQUIRED => $this->t('Required'),
      ],
    ];

    $form['display_submitted'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display author and date information'),
      '#default_value' => $this->config('node_type_defaults.settings')->get('display_submitted') ?? 0,
      '#description' => $this->t('Author username and publish date will be displayed.'),
    ];

    $menu_options = array_map(function (MenuInterface $menu) {
        return $menu->label();
      },
      Menu::loadMultiple()
    );
    asort($menu_options);
    $form['menu_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Available menus'),
      '#default_value' => $this->config('node_type_defaults.settings')->get('menu_options') ?? [],
      '#options' => $menu_options,
      '#description' => $this->t('Default menus that should be enabled.'),
    ];

    // Default publishing options.
    // 'Published' and 'Create new revision' options are enabled by default.
    $default_publishing_options = $this->config('node_type_defaults.settings')->get('publishing_options') ?? [
      'status' => 'status',
      'revision' => 'revision',
    ];

    $form['publishing_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Publishing options'),
      '#default_value' => $default_publishing_options,
      '#options' => [
        'status' => $this->t('Published'),
        'promote' => $this->t('Promoted to front page'),
        'sticky' => $this->t('Sticky at top of lists'),
        'revision' => $this->t('Create new revision'),
      ],
      '#description' => $this->t('Default publishing options that should be enabled.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('node_type_defaults.settings')
      ->set('preview_mode', $form_state->getValue('preview_mode'))
      ->set('menu_options', $form_state->getValue('menu_options'))
      ->set('display_submitted', $form_state->getValue('display_submitted'))
      ->set('publishing_options', $form_state->getValue('publishing_options'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
